var pack = require('./package.json');
var typescript = require('rollup-plugin-typescript');
var json = require('rollup-plugin-json');

module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        version: pack.version,
        tslint: {
            options: {
                configuration: grunt.file.readJSON('tslint.json')
            },
            main: {
                src: ['src/**/*.ts']
            }
        },
        tsfmt: {
            options: {
                configuration: grunt.file.readJSON('tsfmt.json')
            },
            files: {
                src: ['src/**/*.ts']
            }
        },
        rollup: {
            options: {
                sourceMap: true,
                plugins: [
                    json(),
                    typescript()
                ]
            },
            main: {
                dest: 'dist/index.js',
                src: 'src/index.ts' // Only one source file is permitted
            },
            editor: {
                dest: 'dist/editor.js',
                src: 'src/editor.ts' // Only one source file is permitted
            }
        },
        clean: ['dist']
    });

    grunt.registerTask('lint', ['tsfmt', 'tslint']);

    grunt.registerTask('default', ['clean', 'lint', 'rollup']);
}
