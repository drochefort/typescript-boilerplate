// Declare global variables
declare const hmheditor: any;

// Import dependencies
import schema from './data/schema.json';

hmheditor.defaultConfig = './data/config.json';

hmheditor.init(schema);
