// Declare global variables
declare const WidgetsCommon: any;

// Import dependencies
import defaultConfig from './data/config.json';

// Instanciate the Widgets Common library
const common: any = new WidgetsCommon();

// Set the default configuration object
common.configData.setDefaultData(defaultConfig);

common.configData.load((data) => {
    // Implement widget business logic here
    console.log('DATA', data);
});
